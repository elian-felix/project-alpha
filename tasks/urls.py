from django.urls import path
from tasks.views import create_task, list_tasks


urlpatterns = [
    # path('', list_projects, name='list_projects'),
    # path('<int:id>/', show_project, name='show_project'),
    path("create/", create_task, name="create_task"),
    path("mine/", list_tasks, name="show_my_tasks"),
]
