from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from accounts.forms import LoginForm, SignUpForm


# Create your views here.
def login_user(request):
    if request.method == "POST":
        login_form = LoginForm(request.POST)
        if login_form.is_valid():
            username = login_form.cleaned_data["username"]
            password = login_form.cleaned_data["password"]

            user = authenticate(request, username=username, password=password)
            if user is not None:
                login(request, user)
                return redirect("list_projects")
    else:
        login_form = LoginForm()

    context = {"login_form": login_form}

    return render(request, "accounts/login.html", context)


def logout_user(request):
    logout(request)
    return redirect("login")


def signup_user(request):
    if request.method == "POST":
        signup_form = SignUpForm(request.POST)
        if signup_form.is_valid():
            username = signup_form.cleaned_data["username"]
            password = signup_form.cleaned_data["password"]
            password_conf = signup_form.cleaned_data["password_confirmation"]

            if password == password_conf:
                user = User.objects.create_user(username, password=password)
                login(request, user)

                return redirect("list_projects")
            else:
                signup_form.add_error("password", "Passwords do not match")
    else:
        signup_form = SignUpForm()
    context = {"signup_form": signup_form}

    return render(request, "accounts/signup.html", context)
